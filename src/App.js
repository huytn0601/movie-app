import './App.css';
import Header from './components/header/header';
import NavBar from './components/navbar/navbar';
import Movies from './pages/movies/movies';
import Searchs from './pages/searchs/searchs';
import Trending from './pages/trending/trending';
import TVShows from './pages/tvshows/tvshows';
import React from 'react';
import { GlobalProvider } from './context/globalState';


import { Container } from '@mui/material';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

function App() {

  return (
    <GlobalProvider>
      <Router>
        <div className="App">
          <Header />
          <Container>
            <Switch>
              <Route path ='/' exact component={Trending} />
              <Route path ='/movies' exact component={Movies} />
              <Route path ='/tvshows' exact component={TVShows} />
              <Route path ='/searchs' exact component={Searchs} />
            </Switch>
          </Container>
          <NavBar />
        </div>
      </Router>
    </GlobalProvider>
    
  );
}

export default App;
