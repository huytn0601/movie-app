import React from "react";

import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import Rating from "@mui/material/Rating";

export default function FilmCard(props) {
    const {
        name, 
        poster,
        overview, 
        rated
    } = props;
    return (
        <Card sx={{ 
            height: 800,
            width: 300
        }}>
          <CardActionArea>
            <CardMedia
              component="img"
              height="500"
              image= {`https://image.tmdb.org/t/p/w300/${poster}`}
              alt="green iguana"
            />
            <CardContent
                sx={{
                    
                }}
            >
                <Typography gutterBottom variant="h5" component="div">
                    {name}
                </Typography>
                <Typography 
                    variant="body2" 
                    color="text.secondary"
                    textAlign= "justify"
                    fontSize = "0.7rem"
                    paddingTop= ""
                >
                {overview}
                </Typography>
                <Typography typography="title" alignContent="center">
                    <Rating
                        name="simple-controlled"
                        value={Math.floor(rated/2)}
                        readOnly
                    /> 
                    <Typography>
                        {rated}
                    </Typography>
                </Typography>
            </CardContent>
          </CardActionArea>
        </Card>
    );
}