import React from "react";
import style from "./header.module.css"

export default function Header() {
    return (
        <div className={style.headerDiv}>
            <h1>THE MOVIE HUB</h1>
            <h3>Movies for all</h3>
        </div>
    ); 
}