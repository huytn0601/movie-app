import React, { useState, useEffect } from "react";


import Box from '@mui/material/Box';
import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import WhatshotIcon from '@mui/icons-material/Whatshot';
import MovieIcon from '@mui/icons-material/Movie';
import TvIcon from '@mui/icons-material/Tv';
import SearchIcon from '@mui/icons-material/Search';

import { useHistory } from 'react-router-dom';


export default function NavBar() {
  const [value, setValue] = useState(0);
  const history = useHistory();

  useEffect(() => {
    if(value === 0) history.push('/')
    else if(value === 1) history.push('/movies')
    else if(value === 2) history.push('/tvshows')
    else history.push('/searchs')
  }, [value, history])
  
  const customStyle = {
    backgroundColor: 'black',
    width: 1/1,
    position: "fixed",
    bottom: 0,
    zIndex: 100
  };

  return (
    <Box>
      <BottomNavigation
        sx = {customStyle}
        showLabels
        value={value}
        onChange={(event, newValue) => {
          setValue(newValue);
        }}
      >
        <BottomNavigationAction 
          label="Trending" 
          icon={<WhatshotIcon />} 
          style ={{color: 'white'}}
        />
        <BottomNavigationAction 
          label="Movies" 
          icon={<MovieIcon />} 
          style ={{color: 'white'}}
        />
        <BottomNavigationAction 
          label="TV Shows" 
          icon={<TvIcon />}
          style ={{color: 'white'}} 
        />
        <BottomNavigationAction 
          label="Search" 
          icon={<SearchIcon />}
          style ={{color: 'white'}}
        />
      </BottomNavigation>
    </Box>
  );
}