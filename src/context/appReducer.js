import React from "react";

export default function AppReducer(state, action) {
    switch(action.type) {
        case 'CHANGE_TRENDING_PAGE':
            return ({
                ...state, 
                trendingPage: action.payload
            })
        case 'CHANGE_MOVIE_PAGE':
            return ({
                ...state,
                moviePage: action.payload
            })
        case 'CHANGE_TV_PAGE':
            return ({
                ...state,
                tvPage: action.payload
            })
        case 'CHANGE_SEARCH_PAGE':
            return ({
                ...state,
                searchPage: action.payload
            })
        case 'CHANGE_SEARCH_VALUE':
            return ({
                ...state,
                searchValue: action.payload
            })
        default:
            return state;
    }
}