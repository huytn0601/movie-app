import React, { useState, useEffect, createContext, useReducer } from "react";
import AppReducer from "./appReducer"

import axios from "axios";

export const GlobalContext = createContext();

const initState = {
    trendingPage: 1,
    moviePage: 1,
    tvPage: 1,
    searchPage: 1
}

const initSearchState = {
    searchValue: ''
}

export function GlobalProvider({children}) {
    const [trendingData, setTrendingData] = useState([]);
    const [movieData, setMovieData] = useState([]);
    const [tvData, setTvData] = useState([]);
    const [searchData, setSearchData] = useState([]);

    const [trendingState, trendingDispatch] = useReducer(AppReducer, initState);
    const [movieState, movieDispatch] = useReducer(AppReducer, initState);
    const [tvState, tvDispatch] = useReducer(AppReducer, initState);
    const [searchState, searchDispatch] = useReducer(AppReducer, initState);

    const [searchValueState, searchValueDispatch] = useReducer(AppReducer, initSearchState);

    const fetchTredingData = async () => {
        const trendingFetchData = await axios.get(
            `https://api.themoviedb.org/3/trending/all/day?api_key=34f980ee41ec50db0fd24713148a4f6a&page=${trendingState.trendingPage}`
        );
        setTrendingData(trendingFetchData.data.results);
    }
    useEffect(() => {
        fetchTredingData();
    }, [trendingState]);

    const fetchMovieData = async () => {
        const movieFetchData = await axios.get(
            `https://api.themoviedb.org/3/discover/movie?api_key=34f980ee41ec50db0fd24713148a4f6a&language=en-US&sort_by=popularity.desc&page=${movieState.moviePage}&with_watch_monetization_types=flatrate`
        );
        setMovieData(movieFetchData.data.results);
    }
    useEffect(() => {
        fetchMovieData();
    }, [movieState]);

    const fetchTvData = async() => {
        const tvFetchData = await axios.get(
            `https://api.themoviedb.org/3/discover/tv?api_key=34f980ee41ec50db0fd24713148a4f6a&language=en-US&sort_by=popularity.desc&page=1&timezone=America%2FNew_York&with_watch_monetization_types=flatrate&with_status=0&with_type=0&page=${tvState.tvPage}`
        );
        setTvData(tvFetchData.data.results);
    }
    useEffect(() => {
        fetchTvData();
    }, [tvState]);

    const fetchSearchData = async() => {
        const searchFetchData = await axios.get(
            `https://api.themoviedb.org/3/search/multi?api_key=34f980ee41ec50db0fd24713148a4f6a&language=en-US&query=${searchValueState.searchValue}&page=${searchState.searchPage}&include_adult=false`
        )
        setSearchData(searchFetchData.data.results);
    }
    useEffect(() => {
        fetchSearchData();
    }, [searchValueState, searchState]);

    function changeTrendingPage(page) {
        trendingDispatch({
            type: 'CHANGE_TRENDING_PAGE',
            payload: page
        })
    }

    function changeMoviePage(page) {
        movieDispatch({
            type: 'CHANGE_MOVIE_PAGE',
            payload: page
        })
    }

    function changeTvPage(page) {
        tvDispatch({
            type: 'CHANGE_TV_PAGE',
            payload: page
        })
    }

    function changeSearchPage(page) {
        searchDispatch({
            type: 'CHANGE_SEARCH_PAGE',
            payload: page
        })
    }


    function changeSearchValue(value) {
        searchValueDispatch({
            type: 'CHANGE_SEARCH_VALUE',
            payload: value
        })
    }
    
    return (
        <GlobalContext.Provider value = {{
            trendingData,
            movieData,
            tvData,
            searchData,
            changeTrendingPage,
            changeMoviePage,
            changeTvPage,
            changeSearchPage,
            changeSearchValue
        }}
        >
            {children}
        </GlobalContext.Provider>
    );
}
