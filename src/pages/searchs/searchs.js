import React, { useContext, useState } from "react";
import style from "./search.module.css";
import { GlobalContext } from "../../context/globalState";

import { 
    TextField,
    Button
} from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import FilmCard from "../../components/filmCard/filmCard.js";
import { Pagination } from "@mui/material";
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';

export default function Searchs() {
    const [searchValue, setSearchValue] = useState("");
    const { searchData, changeSearchPage, changeSearchValue } = useContext(GlobalContext);

    const handleChange = (event, value) => {
        changeSearchPage(value);
    };

    return (
        <div className={style.searchPage}>
            <div className= {style.searchFormDiv}>
                <TextField
                    style={{ flex: 1, width: 500 }}
                    className="searchBox"
                    label="Search"
                    variant="filled"
                    onChange={(e) => setSearchValue(e.target.value)}
                />
                <Button
                    onClick={() => changeSearchValue(searchValue)}
                    variant="contained"
                    style={{ marginLeft: 10, }}
                >
                    <SearchIcon fontSize="large" />
                </Button>
                <div className={style.searchContent}>
                    <Box sx={{ flexGrow: 1, m: 2 }}>
                      <Grid container spacing={{ xs: 4, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
                        {searchData?.map((value, index) => (
                          <Grid item xs={2} sm={4} md={4} key={index}>
                            <FilmCard 
                                name = {value.original_title || value.original_name}
                                poster = {value.poster_path}
                                overview = {value.overview}
                                rated = {value.vote_average}
                            />
                          </Grid>
                        ))}
                      </Grid>
                    </Box>
                    <Box sx={{  paddingBottom:20 }}>
                      <Pagination 
                        count = {10} 
                        onChange={handleChange}
                        color ="secondary"
                      />
                    </Box>
                </div>
            </div>
        </div>
    );
}