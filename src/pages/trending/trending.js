import React, { useContext } from "react";
import { GlobalContext } from "../../context/globalState";

import FilmCard from "../../components/filmCard/filmCard.js";
import { Pagination } from "@mui/material";
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';

export default function Trending(props) {
    const { trendingData, changeTrendingPage } = useContext(GlobalContext);

    const handleChange = (event, value) => {
      changeTrendingPage(value);
    };

    return (
      <div>
        <h1> Trending </h1>
        <Box sx={{ flexGrow: 1, m: 2 }}>
          <Grid container spacing={{ xs: 4, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
            {trendingData?.map((value, index) => (
              <Grid item xs={2} sm={4} md={4} key={index}>
                <FilmCard 
                    name = {value.original_title || value.original_name}
                    poster = {value.poster_path}
                    overview = {value.overview}
                    rated = {value.vote_average}
                />
              </Grid>
            ))}
          </Grid>
        </Box>
        <Box sx={{  paddingBottom:20 }}>
          <Pagination 
            count = {10} 
            onChange={handleChange}
            color ="secondary"
          />
        </Box>
      </div>
        
    );
}